# imports
import requests
import time
import os
import msal
import atexit
import logging
import json

# Set polling interval
poll_interval = 60

# Azure AD Application parameters
default_client_id = "e8233eee-02bb-45f1-ba41-844a2834c716"
default_tenant_id = "common"
client_id = None
tenant_id = None
scope = ["User.read", "Presence.read"]

def authorizeApp(client_id, scope):
    flow = app.initiate_device_flow(scopes=scope)
    if "user_code" not in flow:
        raise ValueError(
            "Fail to create device flow. Err: %s" % json.dumps(flow, indent=4))

    print(flow["message"])
    #sys.stdout.flush()  # Some terminal needs this to ensure the message is shown

    # Ideally you should wait here, in order to save some unnecessary polling
    # input("Press Enter after signing in from another device to proceed, CTRL+C to abort.")

    result = app.acquire_token_by_device_flow(flow)  # By default it will block
        # You can follow this instruction to shorten the block time
        #    https://msal-python.readthedocs.io/en/latest/#msal.PublicClientApplication.acquire_token_by_device_flow
        # or you may even turn off the blocking behavior,
        # and then keep calling acquire_token_by_device_flow(flow) in your own customized loop.
    

    if "access_token" in result:
        # Write token to the cache-file
        open("token_cache.bin", "w").write(token_cache.serialize())
        # Get the expiretime and calculate the timestamp where this token expires
        expires_in = result.get("expires_in")
        expire_timestamp = int(time.time()+expires_in)
        result["expire_timestamp"] = expire_timestamp
        return result
    else:
        print(result.get("error"))
        print(result.get("error_description"))
        print(result.get("correlation_id"))  # You may need this when reporting a bug
        return False


def pollPresence(token_json):
    #with open('auth.json', 'r') as f:
    #    token_json = json.load(f)
    #
    token = token_json["access_token"]
    
    presence_url = "https://graph.microsoft.com/beta/me/presence"
    #craft request headers
    headers = { 'Authorization' : "Bearer %s" %token }
    
    # Make the request
    req = requests.get(presence_url, headers=headers)

    # storing the JSON response 
    # from url in data
    data_json = json.loads(req.text)

    availability = data_json.get('availability')
    activity = data_json.get('activity')

    return availability, activity

def checkTokenExpiry(offset, token):
    current_time = time.time()    
    expiry_time = token.get("expire_timestamp")
    
    if expiry_time < (current_time - offset):
        return True
    else:
        return False

def checkTokenCache(scope):
    accounts = app.get_accounts()
    if accounts:
        #logging.info("Account(s) exists in cache, probably with token too. Let's try.")
        #print("Pick the account you want to use to proceed:")
        #for a in accounts:
        #    print(a)
        # Assuming the end user chose this one
        chosen = accounts[0]
        # Now let's try to find a token in cache for this account
        result = app.acquire_token_silent(scope, account=chosen)
        expires_in = result.get("expires_in")
        expire_timestamp = int(time.time()+expires_in)
        result["expire_timestamp"] = expire_timestamp
        return result
    else:
        return False

#Check if custom tenant and client id are provided
if client_id == None:
    client_id = default_client_id
if tenant_id == None:
    tenant_id = default_tenant_id

# Instantiate MSAL Cache:
token_cache = msal.SerializableTokenCache()
if os.path.exists("token_cache.bin"):
    token_cache.deserialize(open("token_cache.bin", "r").read())
atexit.register(lambda:
    open("token_cache.bin", "w").write(token_cache.serialize())
    # Hint: The following optional line persists only when state changed
    if token_cache.has_state_changed else None
    )

# Instantiate MSAL app
app = msal.PublicClientApplication(client_id=client_id, token_cache=token_cache)

# Check first if there is an earlier user token present
token = checkTokenCache(scope)

# If not, run a new authorization flow
if not token:
    token = authorizeApp(client_id, scope)

# If we got a token, poll user presence to infinity
while token:
    # Check if the token needs to be refreshed
    if checkTokenExpiry(poll_interval, token):
        checkTokenCache(scope)
    print(pollPresence(token))
    time.sleep(poll_interval)
    
#If the script gets here, something went wrong?
logging.error("You shouldn't see this")